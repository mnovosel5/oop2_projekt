#ifndef inputFormH
#define inputFormH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>

class Tsize_form : public TForm {
    __published:
		TButton *startButton;
        TMainMenu *main_menu;
        TMenuItem *N1;
        TRadioGroup *difficulty_group;
        TMenuItem *Settings1;
		TLabel *welcome_label;
		TButton *Button1;
	TButton *captionButton;
	TPanel *Panel1;
		void __fastcall N1Click(TObject *Sender);
		void __fastcall FormCreate(TObject *Sender);
		void __fastcall Settings1Click(TObject *Sender);
		void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
		void __fastcall startButtonClick(TObject *Sender);
		void __fastcall Button1Click(TObject *Sender);
    private:
    public:
        __fastcall Tsize_form(TComponent* Owner, UnicodeString username);
        UnicodeString username;
};

extern PACKAGE Tsize_form *size_form;

#endif
