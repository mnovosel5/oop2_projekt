#ifndef mainH
#define mainH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Dialogs.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include "timerThread.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <map>
#include <vector>

class Tmain_form: public TForm {
    __published:
		// IDE-managed Components
        TGroupBox *information_group_box;
        TLabel *Label1;
        TEdit *current_pairs;
        TLabel *out_of_label;
        TEdit *max_pairs;
        TButton *Button3;
        TButton *Button4;
	TADOConnection *Connection;
	TADOTable *TableUsers;
	TDataSource *DataUsers;
	TADOTable *TableDifficulties;
	TADOTable *TableScores;
	TDataSource *DataDifficulties;
	TDataSource *DataScores;
	TButton *Button1;
	TButton *Button2;
	TImage *Image1;
	TEdit *elapsedTimeEdit;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall card_click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
    private:
		// User declarations
    public:
		// User declarations
        __fastcall Tmain_form(TComponent* Owner, UnicodeString _username, int _difficulty);
        // Thread for the timer
        TimerThread *timerThread;
        // DLL file
        HINSTANCE cards_dll;
        // Functions for turning over cards
        void turn_over(TImage *card);
        void totalTurnOver();
        std::vector<TImage*> cardObjects;
        // Card identification vectors
        std::map<UnicodeString, int> cardTags;
		std::map<UnicodeString, int> guessedCards;
        // Number of rows and columns
        int rows, columns;
        // Number of correctly guessed pairs
        int pairs;
        // Control over turned cards
        bool turned;
        // Card back ID
        int turned_over;
        // Card objects for tracking which two cards were selected
        TImage *card_0;
        TImage *pom;
        // Cleanup function
        void cleanUp();
        // Username
        UnicodeString username;
        // Difficulty
        int difficulty;
        // Save player function
        void savePlayer();
        // Card background
        int turnedOver;
};

extern PACKAGE Tmain_form *main_form;

#endif
