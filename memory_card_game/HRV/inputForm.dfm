object size_form: Tsize_form
  Left = 0
  Top = 0
  Caption = 'Memorija'
  ClientHeight = 491
  ClientWidth = 434
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = main_menu
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object welcome_label: TLabel
    Left = 120
    Top = 56
    Width = 176
    Height = 23
    Caption = 'Dobrodosli, osobo!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object startButton: TButton
    Left = 120
    Top = 210
    Width = 75
    Height = 25
    Caption = 'ZAPOCNI'
    TabOrder = 0
    OnClick = startButtonClick
  end
  object difficulty_group: TRadioGroup
    Left = 120
    Top = 91
    Width = 201
    Height = 105
    Caption = 'Tezina (Broj karti)'
    ItemIndex = 0
    Items.Strings = (
      '12'
      '24'
      '36')
    TabOrder = 1
  end
  object Button1: TButton
    Left = 246
    Top = 210
    Width = 75
    Height = 25
    Caption = 'NATRAG'
    TabOrder = 2
    OnClick = Button1Click
  end
  object captionButton: TButton
    Left = 120
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Dobrodosli, '
    TabOrder = 3
  end
  object Panel1: TPanel
    Left = 120
    Top = 241
    Width = 201
    Height = 106
    TabOrder = 4
  end
  object main_menu: TMainMenu
    Left = 18
    Top = 8
    object N1: TMenuItem
      Caption = 'Rezultati'
      OnClick = N1Click
    end
    object Settings1: TMenuItem
      Caption = 'Postavke'
      OnClick = Settings1Click
    end
  end
end
