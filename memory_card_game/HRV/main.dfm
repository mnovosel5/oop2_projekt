object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'Memorija'
  ClientHeight = 383
  ClientWidth = 531
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Microsoft Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 8
    Top = 8
    Width = 1000
    Height = 1000
    Stretch = True
  end
  object information_group_box: TGroupBox
    Left = 154
    Top = 8
    Width = 250
    Height = 125
    Caption = 'Napredak'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Microsoft Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 40
      Top = 30
      Width = 26
      Height = 13
      Caption = 'Parovi:'
    end
    object out_of_label: TLabel
      Left = 111
      Top = 30
      Width = 27
      Height = 13
      Caption = 'od'
    end
    object current_pairs: TEdit
      Left = 73
      Top = 27
      Width = 32
      Height = 21
      Alignment = taCenter
      Enabled = False
      TabOrder = 0
    end
    object max_pairs: TEdit
      Left = 146
      Top = 27
      Width = 32
      Height = 21
      Alignment = taCenter
      Enabled = False
      TabOrder = 1
    end
    object Button3: TButton
      Left = 40
      Top = 81
      Width = 75
      Height = 25
      Caption = 'Ponovi'
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button4: TButton
      Left = 132
      Top = 81
      Width = 75
      Height = 25
      Caption = 'Izadi'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Microsoft Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = Button2Click
    end
    object Button2: TButton
      Left = -80
      Top = 88
      Width = 75
      Height = 25
      Caption = 'CRO'
      TabOrder = 4
    end
    object elapsedTimeEdit: TEdit
      Left = 40
      Top = 54
      Width = 169
      Height = 21
      TabOrder = 5
    end
  end
  object Button1: TButton
    Left = 8
    Top = 371
    Width = 1
    Height = 4
    Caption = 'Pobjedili ste!'
    Enabled = False
    TabOrder = 1
  end
  object Connection: TADOConnection
    LoginPrompt = False
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 264
    Top = 144
  end
  object TableUsers: TADOTable
    Connection = Connection
    CursorType = ctStatic
    TableName = 'Users'
    Left = 208
    Top = 200
  end
  object DataUsers: TDataSource
    DataSet = TableUsers
    Enabled = False
    Left = 312
    Top = 200
  end
  object TableDifficulties: TADOTable
    Connection = Connection
    CursorType = ctStatic
    TableName = 'Difficulties'
    Left = 208
    Top = 256
  end
  object TableScores: TADOTable
    Connection = Connection
    CursorType = ctStatic
    TableName = 'Scores'
    Left = 208
    Top = 312
  end
  object DataDifficulties: TDataSource
    DataSet = TableDifficulties
    Enabled = False
    Left = 312
    Top = 256
  end
  object DataScores: TDataSource
    DataSet = TableScores
    Enabled = False
    Left = 312
    Top = 312
  end
end
