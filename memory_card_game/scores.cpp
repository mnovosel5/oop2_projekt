
// ********************************************************************************************************** //
//                                                                                                          
//                                             XML Data Binding                                             
//                                                                                                          
//         Generated on: 4/26/2019 12:17:12 AM                                                              
//       Generated from: C:\Users\mnovosel2\Desktop\repositories\oop2_projekt\memory_card_game\scores.xml   
//   Settings stored in: C:\Users\mnovosel2\Desktop\repositories\oop2_projekt\memory_card_game\scores.xdb   
//                                                                                                          
// ********************************************************************************************************** //

#include <System.hpp>
#pragma hdrstop

#include "scores.h"


// Global Functions 

_di_IXMLscoresType __fastcall Getscores(Xml::Xmlintf::_di_IXMLDocument Doc)
{
  return (_di_IXMLscoresType) Doc->GetDocBinding("scores", __classid(TXMLscoresType), TargetNamespace);
};

_di_IXMLscoresType __fastcall Getscores(Xml::Xmldoc::TXMLDocument *Doc)
{
  Xml::Xmlintf::_di_IXMLDocument DocIntf;
  Doc->GetInterface(DocIntf);
  return Getscores(DocIntf);
};

_di_IXMLscoresType __fastcall Loadscores(const System::UnicodeString& FileName)
{
  return (_di_IXMLscoresType) Xml::Xmldoc::LoadXMLDocument(FileName)->GetDocBinding("scores", __classid(TXMLscoresType), TargetNamespace);
};

_di_IXMLscoresType __fastcall  Newscores()
{
  return (_di_IXMLscoresType) Xml::Xmldoc::NewXMLDocument()->GetDocBinding("scores", __classid(TXMLscoresType), TargetNamespace);
};

// TXMLscoresType 

void __fastcall TXMLscoresType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("player"), __classid(TXMLplayerType));
  ItemTag = "player";
  ItemInterface = __uuidof(IXMLplayerType);
  Xml::Xmldoc::TXMLNodeCollection::AfterConstruction();
};

_di_IXMLplayerType __fastcall TXMLscoresType::Get_player(int Index)
{
  return (_di_IXMLplayerType) List->Nodes[Index];
};

_di_IXMLplayerType __fastcall TXMLscoresType::Add()
{
  return (_di_IXMLplayerType) AddItem(-1);
};

_di_IXMLplayerType __fastcall TXMLscoresType::Insert(const int Index)
{
  return (_di_IXMLplayerType) AddItem(Index);
};

// TXMLplayerType 

System::UnicodeString __fastcall TXMLplayerType::Get_username()
{
  return GetChildNodes()->Nodes[System::UnicodeString("username")]->Text;
};

void __fastcall TXMLplayerType::Set_username(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("username")]->NodeValue = Value;
};

float __fastcall TXMLplayerType::Get_score()
{
  return XmlStrToFloatExt(GetChildNodes()->Nodes[System::UnicodeString("score")]->Text);
};

void __fastcall TXMLplayerType::Set_score(float Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("score")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLplayerType::Get_difficulty()
{
  return GetChildNodes()->Nodes[System::UnicodeString("difficulty")]->Text;
};

void __fastcall TXMLplayerType::Set_difficulty(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("difficulty")]->NodeValue = Value;
};

// TXMLplayerTypeList 

_di_IXMLplayerType __fastcall TXMLplayerTypeList::Add()
{
  return (_di_IXMLplayerType) AddItem(-1);
};

_di_IXMLplayerType __fastcall TXMLplayerTypeList::Insert(const int Index)
{
  return (_di_IXMLplayerType) AddItem(Index);
};

_di_IXMLplayerType __fastcall TXMLplayerTypeList::Get_Item(int Index)
{
  return (_di_IXMLplayerType) List->Nodes[Index];
};
