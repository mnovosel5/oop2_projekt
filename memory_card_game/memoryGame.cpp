#include <vcl.h>
#pragma hdrstop
#include <tchar.h>

#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("scoresForm.cpp", score_form);
USEFORM("settingsForm.cpp", Settings);
USEFORM("inputForm.cpp", size_form);
USEFORM("loginWindow.cpp", login_form);
USEFORM("main.cpp", main_form);
USEFORM("DataModule.cpp", MemoryDataModule); /* TDataModule: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int) {
	try {
		Application->Initialize();
		Application->MainFormOnTaskBar = true;
		TStyleManager::TrySetStyle("Windows10");
		Application->CreateForm(__classid(Tlogin_form), &login_form);
		Application->CreateForm(__classid(TSettings), &Settings);
		Application->CreateForm(__classid(TMemoryDataModule), &MemoryDataModule);
		Application->Run();
	} catch(Exception &exception) {
		Application->ShowException(&exception);
	} catch(...) {
		try {
			throw Exception("");
		} catch(Exception &exception) {
			Application->ShowException(&exception);
		}
	}
	return 0;
}
