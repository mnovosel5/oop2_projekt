#include <vcl.h>
#pragma hdrstop

#include "inputForm.h"
#include "main.h"
#include "scoresForm.h"
#include "settingsForm.h"
#include "loginWindow.h"
#include <memory>

#pragma package(smart_init)
#pragma resource "*.dfm"
Tsize_form *size_form;

__fastcall Tsize_form::Tsize_form(TComponent* Owner, UnicodeString _username): TForm(Owner) {
    username = _username;
}

void __fastcall Tsize_form::N1Click(TObject *Sender) {
    Tscore_form *scores_form = new Tscore_form(this);
    scores_form->ShowModal();
    delete scores_form;
}

void __fastcall Tsize_form::FormCreate(TObject *Sender) {
	welcome_label->Caption = captionButton->Caption + username + "!";
}

void __fastcall Tsize_form::Settings1Click(TObject *Sender) {
    TSettings *settings_form = new TSettings(this, username);
    settings_form->ShowModal();
    delete settings_form;
}

void __fastcall Tsize_form::FormClose(TObject *Sender, TCloseAction &Action) {
	Application->MainForm->Close();
}

void __fastcall Tsize_form::startButtonClick(TObject *Sender) {
    Tmain_form *game = new Tmain_form(NULL, username, StrToInt(difficulty_group->Items->Strings[difficulty_group->ItemIndex]));
    game->ShowModal();
}

void __fastcall Tsize_form::Button1Click(TObject *Sender) {
    login_form->Show();
    Hide();
}
