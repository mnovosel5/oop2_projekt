#ifndef settingsFormH
#define settingsFormH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>

class TSettings: public TForm {
    __published:
        TGroupBox *cardback_group;
        TLabel *settings_label;
		TImage *card_image;
        TButton *button_left;
        TButton *button_right;
        TGroupBox *font_group;
        TButton *font_button;
        TLabel *font_name_label;
        TLabel *font_size_label;
        TButton *save_button;
	TButton *Button1;
	TGroupBox *GroupBox1;
	TButton *Button2;
	TColorDialog *color_dialog;
	TFontDialog *font_dialog;
	TButton *Button4;
	TPanel *Panel2;
	TImage *Image1;
	TComboBox *ComboBox1;
	TEdit *Edit1;
	TEdit *Edit2;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall button_leftClick(TObject *Sender);
        void __fastcall button_rightClick(TObject *Sender);
        void __fastcall font_buttonClick(TObject *Sender);
	void __fastcall save_buttonClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall ComboBox1Change(TObject *Sender);
    private:
    public:
        __fastcall TSettings(TComponent* Owner, UnicodeString _username);
        int currently_selected;
        UnicodeString username;
};

extern PACKAGE TSettings *Settings;

#endif
