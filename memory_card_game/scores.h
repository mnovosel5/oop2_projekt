
// ********************************************************************************************************** //
//                                                                                                          
//                                             XML Data Binding                                             
//                                                                                                          
//         Generated on: 4/26/2019 12:17:12 AM                                                              
//       Generated from: C:\Users\mnovosel2\Desktop\repositories\oop2_projekt\memory_card_game\scores.xml   
//   Settings stored in: C:\Users\mnovosel2\Desktop\repositories\oop2_projekt\memory_card_game\scores.xdb   
//                                                                                                          
// ********************************************************************************************************** //

#ifndef   scoresH
#define   scoresH

#include <System.hpp>
#include <Xml.Xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Xml.XMLDoc.hpp>
#include <XMLNodeImp.h>
#include <Xml.xmlutil.hpp>


// Forward Decls 

__interface IXMLscoresType;
typedef System::DelphiInterface<IXMLscoresType> _di_IXMLscoresType;
__interface IXMLplayerType;
typedef System::DelphiInterface<IXMLplayerType> _di_IXMLplayerType;
__interface IXMLplayerTypeList;
typedef System::DelphiInterface<IXMLplayerTypeList> _di_IXMLplayerTypeList;

// IXMLscoresType 

__interface INTERFACE_UUID("{F6E10D70-4E0E-46D6-980D-5FC1C8A380E1}") IXMLscoresType : public Xml::Xmlintf::IXMLNodeCollection
{
public:
public:
  // Property Accessors 
  virtual _di_IXMLplayerType __fastcall Get_player(int Index) = 0;
  // Methods & Properties 
  virtual _di_IXMLplayerType __fastcall Add() = 0;
  virtual _di_IXMLplayerType __fastcall Insert(const int Index) = 0;
  __property _di_IXMLplayerType player[int Index] = { read=Get_player };/* default */
};

// IXMLplayerType 

__interface INTERFACE_UUID("{1D64D848-9011-4A2A-89FA-D54D3CF060F5}") IXMLplayerType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_username() = 0;
  virtual float __fastcall Get_score() = 0;
  virtual System::UnicodeString __fastcall Get_difficulty() = 0;
  virtual void __fastcall Set_username(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_score(float Value) = 0;
  virtual void __fastcall Set_difficulty(System::UnicodeString Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString username = { read=Get_username, write=Set_username };
  __property float score = { read=Get_score, write=Set_score };
  __property System::UnicodeString difficulty = { read=Get_difficulty, write=Set_difficulty };
};

// IXMLplayerTypeList 

__interface INTERFACE_UUID("{AF09A300-2397-4E24-B83A-124F6FF3EA61}") IXMLplayerTypeList : public Xml::Xmlintf::IXMLNodeCollection
{
public:
  // Methods & Properties 
  virtual _di_IXMLplayerType __fastcall Add() = 0;
  virtual _di_IXMLplayerType __fastcall Insert(const int Index) = 0;

  virtual _di_IXMLplayerType __fastcall Get_Item(int Index) = 0;
  __property _di_IXMLplayerType Items[int Index] = { read=Get_Item /* default */ };
};

// Forward Decls 

class TXMLscoresType;
class TXMLplayerType;
class TXMLplayerTypeList;

// TXMLscoresType 

class TXMLscoresType : public Xml::Xmldoc::TXMLNodeCollection, public IXMLscoresType
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLscoresType 
  virtual _di_IXMLplayerType __fastcall Get_player(int Index);
  virtual _di_IXMLplayerType __fastcall Add();
  virtual _di_IXMLplayerType __fastcall Insert(const int Index);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLplayerType 

class TXMLplayerType : public Xml::Xmldoc::TXMLNode, public IXMLplayerType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLplayerType 
  virtual System::UnicodeString __fastcall Get_username();
  virtual float __fastcall Get_score();
  virtual System::UnicodeString __fastcall Get_difficulty();
  virtual void __fastcall Set_username(System::UnicodeString Value);
  virtual void __fastcall Set_score(float Value);
  virtual void __fastcall Set_difficulty(System::UnicodeString Value);
};

// TXMLplayerTypeList 

class TXMLplayerTypeList : public Xml::Xmldoc::TXMLNodeCollection, public IXMLplayerTypeList
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLplayerTypeList 
  virtual _di_IXMLplayerType __fastcall Add();
  virtual _di_IXMLplayerType __fastcall Insert(const int Index);

  virtual _di_IXMLplayerType __fastcall Get_Item(int Index);
};

// Global Functions 

_di_IXMLscoresType __fastcall Getscores(Xml::Xmlintf::_di_IXMLDocument Doc);
_di_IXMLscoresType __fastcall Getscores(Xml::Xmldoc::TXMLDocument *Doc);
_di_IXMLscoresType __fastcall Loadscores(const System::UnicodeString& FileName);
_di_IXMLscoresType __fastcall  Newscores();

#define TargetNamespace ""

#endif