object MemoryDataModule: TMemoryDataModule
  OldCreateOrder = False
  Height = 214
  Width = 250
  object Connection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=resul' +
      'ts.mdb;Mode=Share Deny None;Persist Security Info=False;Jet OLED' +
      'B:System database="";Jet OLEDB:Registry Path="";Jet OLEDB:Databa' +
      'se Password="";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Lockin' +
      'g Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bu' +
      'lk Transactions=1;Jet OLEDB:New Database Password="";Jet OLEDB:C' +
      'reate System Database=False;Jet OLEDB:Encrypt Database=False;Jet' +
      ' OLEDB:Don'#39't Copy Locale on Compact=False;Jet OLEDB:Compact With' +
      'out Replica Repair=False;Jet OLEDB:SFP=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 104
    Top = 32
  end
  object TableUsers: TADOTable
    Active = True
    Connection = Connection
    CursorType = ctStatic
    TableName = 'Users'
    Left = 32
    Top = 88
  end
  object TableDifficulties: TADOTable
    Active = True
    Connection = Connection
    CursorType = ctStatic
    TableName = 'Difficulties'
    Left = 104
    Top = 88
  end
  object TableScores: TADOTable
    Active = True
    Connection = Connection
    CursorType = ctStatic
    TableName = 'Scores'
    Left = 184
    Top = 88
  end
  object DataUsers: TDataSource
    DataSet = TableUsers
    Left = 32
    Top = 144
  end
  object DataDifficulties: TDataSource
    DataSet = TableDifficulties
    Left = 104
    Top = 144
  end
  object DataScores: TDataSource
    DataSet = TableScores
    Left = 184
    Top = 144
  end
end
