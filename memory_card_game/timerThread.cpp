#include <System.hpp>
#pragma hdrstop

#include "timerThread.h"
#include "main.h"
#pragma package(smart_init)

__fastcall TimerThread::TimerThread(bool CreateSuspended, TEdit *_editReference, bool _english)
	: TThread(CreateSuspended), editReference(_editReference), english(_english) {
    timer = new TTimer(NULL);
    timer->Interval = 100;
    timer->OnTimer = timerTrigger;
    timer->Enabled = false;
    elapsedTime = 0;
}

void __fastcall TimerThread::timerTrigger(TObject *Sender) {
   	elapsedTime += 100;
    UnicodeString text;
    if(english) {
        text = "Elapsed time: ";
	} else {
        text = "Provedeno vrijeme: ";
	}
	Synchronize([&](){
        editReference->Text = text + FloatToStr(elapsedTime / 1000.) + "s";
	});
}

void __fastcall TimerThread::Execute() {
    timer->Enabled = true;
}
