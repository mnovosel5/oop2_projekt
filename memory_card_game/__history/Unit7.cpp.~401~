#include<vcl.h>
#include<vector>
#include<random>
#include<algorithm>
#include<ctime>
#include<cstdlib>
#include<map>
#include<string>

#pragma hdrstop

#include "Unit7.h"
#include "Unit1.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

TForm7 *Form7;
HINSTANCE cards_dll;

// Card object vector
std::vector<TImage*> card_objects;

// Map for tracking which card is which
std::map<UnicodeString, int> card_tags;
std::map<UnicodeString, int> guessed_cards;

/*

    e.g.

        card_tags["card_0"] = 51;
        card_tags["card_1"] = 43;
        <...>

*/

// Number of rows and columns
int rows, columns;

// Number of correctly guessed pairs
int pairs;

// Card objects for tracking which two cards were selected
TImage *card_0;
bool turned;
int turned_over; // Card back

#define card_width 71
#define card_height 96

__fastcall TForm7::TForm7(TComponent* Owner): TForm(Owner) {

}

/*

    Range is 1 to 13 - to 52

    1 - Ace
    2 - 2
    3 - 3
    4 - 4
    5 - 5
    6 - 6
    7 - 7
    8 - 8
    9 - 9
    10 - 10
    11 - J
    12 - Q
    13 - K

    Dimensions:
        width - 71
        height - 96

*/

// Generate random integer from <low> to <high>, inclusive
int rand_int(int low, int high) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(low, high);
    return(dist6(rng));
}

// Generate the card values randomly and insert them into a vector
std::vector<int> random_layout() {
    std::vector<int> numbers;
    for(int i = 0; i < ((rows * columns) / 2); i++) {
        int generated_number = rand_int(1, 52);
        // If the number had already been generated, keep creating new ones
        while(std::find(numbers.begin(), numbers.end(), generated_number) != numbers.end()) {
            generated_number = rand_int(1, 52);
		}
		numbers.push_back(generated_number);
        numbers.push_back(generated_number);
	}
    std::srand(unsigned(std::time(0)));
    std::random_shuffle(numbers.begin(), numbers.end());
    return(numbers);
}

void turn_over(TImage *card) {
    if(card->Tag == 0) {
        card->Tag = card_tags[card->Name];
        card->Picture->Bitmap->LoadFromResourceID((int)cards_dll, card->Tag);
	} else {
        card->Tag = 0;
        card->Picture->Bitmap->LoadFromResourceID((int)cards_dll, turned_over);
	}
}

void total_turn_over() {
    for(int i = 0; i < card_objects.size(); i++) {
        turn_over(card_objects[i]);
	}
}

void __fastcall TForm7::card_click(TObject *Sender) {
    TImage *pom = dynamic_cast<TImage*>(Sender);
    if((turned && pom->Tag == card_0->Tag) || guessed_cards[pom->Name]) {
        return;
	}
    if(!turned) {
        turned = !turned;
        card_0 = pom;
        turn_over(pom);
	} else {
        turned = !turned;
        turn_over(pom);
        if(card_tags[card_0->Name] == card_tags[pom->Name]) {
            guessed_cards[card_0->Name] = 1;
            guessed_cards[pom->Name] = 1;
            current_pairs->Text = ++pairs;
        } else {
            /*
            	Call ProcessMessages to permit the application to process messages that are currently in the message queue.
				ProcessMessages cycles the Windows message loop until it is empty, and then returns control to the application.
            */
			Application->ProcessMessages();
            Sleep(350);
            turn_over(pom);
            turn_over(card_0);
		}
        card_0 = NULL;
	}
}

bool validate(int rows, int columns) {
    bool correct = true;
    if((rows * columns) % 2 || rows <= 2 || columns <= 2) {
        correct = false;
	}
    return(correct);
}

void __fastcall TForm7::FormCreate(TObject *Sender) {
    card_0 = new TImage(this);
    card_0->Tag = -1;
    turned = false;
    pairs = 0;

    // Number of columns and rows
    Tsize_form *size_form = new Tsize_form(this);
    size_form->ShowModal();
	rows = StrToInt(size_form->row_edit->Text); // Rows
    columns = StrToInt(size_form->column_edit->Text); // Columns
    UnicodeString card_back = size_form->radio_group->Items->Strings[size_form->radio_group->ItemIndex];

    if(card_back == "classic") {
        turned_over = 53;
    } else if(card_back == "royal") {
        turned_over = 54;
	} else {
        turned_over = 59;
    }

    if(!validate(rows, columns))
        ShowMessage("Invalid input, exiting!");
        Application->Terminate();
	}

    delete size_form;

    // Pairs
    max_pairs->Text = (rows * columns) / 2;
    current_pairs->Text = 0;

    // Load the .dll file
    if((cards_dll = LoadLibrary(L"cards.dll")) == NULL) {
        ShowMessage("Cannot load .dll file!");
        Application->Terminate();
	}

    // Set form dimensions
	Form7->Height = (card_height + 1) * (columns + 1);
    Form7->Height += 25;
    Form7->Width = (card_width + 1) * (rows + 1);

    // Random layout vector
    std::vector<int> layout = random_layout();
    card_objects.resize(rows * columns);

    // Add the cards
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < columns; j++) {
            int index = i * columns + j;
            card_objects[index] = new TImage(this);
            card_objects[index]->Parent = this;
            card_objects[index]->Left = 16 + i * 78;
            card_objects[index]->Top = 15 + 16 + j * 102;
            card_objects[index]->AutoSize = true;
            card_objects[index]->OnClick = card_click;
            card_objects[index]->Picture->Bitmap->LoadFromResourceID((int)cards_dll, layout[index]);
            card_objects[index]->Name = "card_" + IntToStr(index);
            // The tag used to identify the card type
            card_tags[card_objects[index]->Name] = layout[index];
            card_objects[index]->Tag = card_tags[card_objects[index]->Name];
            guessed_cards[card_objects[index]->Name] = 0;
        }
	}

    total_turn_over();
}

void __fastcall TForm7::FormClose(TObject *Sender, TCloseAction &Action) {
    // Delete every instance of the TImage objects
    for(int i = 0; i < card_objects.size(); i++) delete(card_objects[i]);
    card_0 = NULL;
    // Free the resources of the .dll file
    FreeLibrary(cards_dll);
}

void __fastcall TForm7::urnthatshitover1Click(TObject *Sender) {
    total_turn_over();
}
