#include <vcl.h>
#pragma hdrstop

#include "loginWindow.h"
#include "inputForm.h"
#include "ctype.h"
#include "reinit.hpp"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tlogin_form *login_form;

bool expand;

void __fastcall Tlogin_form::update_combo() {
	int remember = profile_combo->ItemIndex;
	profile_combo->Clear();
	table_user->First();
  	for(int i = 0; i < v->RecordCount; i++) {
		profile_combo->Items->Add(table_user->FieldByName("Username")->Value);
		table_user->Next();
  	}
	profile_combo->ItemIndex = remember;
}

__fastcall Tlogin_form::Tlogin_form(TComponent* Owner): TForm(Owner) {
    expand = true;
    control_image->ShowHint = true;
    control_image->Picture->LoadFromFile("red.png");
	valid = false;
}

void __fastcall Tlogin_form::validate() {
    UnicodeString empty_message;
	UnicodeString length_message;
	UnicodeString character_message;
	UnicodeString exists_message;

    empty_message = Button2->Caption;
    length_message = Button3->Caption;
    character_message = Button4->Caption;
    exists_message = Button5->Caption;
    UnicodeString empty = "";

    UnicodeString username = Trim(username_edit->Text);

    bool invalid_length = false;
    bool empty_input = false;
    bool invalid_characters = false;
	bool exists_in_database = false;

	TLocateOptions Opts;     
	Opts.Clear();
	Opts << loCaseInsensitive; 
	if(table_user->Locate("Username", username, Opts)) {
		exists_in_database = true;
    }

    if(username == "") {
        empty_input = true;
	}

    if(username.Length() > 12 || username.Length() < 3) {
		invalid_length = true;
	}

	for(int i = 1; i <= username.Length(); i++) {
    	if(!isalnum(username[i])) {
        	invalid_characters = true;
			break;
		}
	}

	if(empty_input || invalid_length || invalid_characters || exists_in_database) {
		UnicodeString hint_message = 
			((empty_input) ? empty_message : empty) +
			((invalid_length) ? length_message : empty) +
        	((invalid_characters) ? character_message : empty) +
			((exists_in_database) ? exists_message : empty);
		valid = false;
		control_image->Hint = hint_message;
		control_image->Picture->LoadFromFile("red.png");
	} else {
		valid = true;
        control_image->Hint = Button6->Caption;
		control_image->Picture->LoadFromFile("green.png");
	}
}

void __fastcall Tlogin_form::username_editChange(TObject *Sender) {
	validate();
}

void __fastcall Tlogin_form::FormCreate(TObject *Sender) {
  	update_combo();
    control_image->Hint = Button2->Caption;
}

void __fastcall Tlogin_form::create_buttonClick(TObject *Sender) {
	if(valid) {
		UnicodeString username = Trim(username_edit->Text);
		table_user->Insert();
    	table_user->FieldByName("Username")->AsString = username;
    	table_user->Post();
		update_combo();
		validate();
	} else {
    	ShowMessage(Button1->Caption);
	}	
}

void __fastcall Tlogin_form::login_buttonClick(TObject *Sender) {
    if(profile_combo->Items->Strings[profile_combo->ItemIndex] == "") {
        return;
	}
	Tsize_form *size_form = new Tsize_form(NULL, profile_combo->Items->Strings[profile_combo->ItemIndex]);
    Hide();
	size_form->ShowModal();
}

void __fastcall Tlogin_form::Timer1Timer(TObject *Sender) {
    int size = titleLabel->Font->Size;
    if(expand) {
        if(size > 30) {
            expand = false;
            titleLabel->Font->Size--;
        } else {
            titleLabel->Font->Size++;
        }
	} else {
        if(size < 20) {
            expand = true;
            titleLabel->Font->Size++;
        } else {
            titleLabel->Font->Size--;
        }
	}
}

void __fastcall Tlogin_form::ComboBox1Change(TObject *Sender) {
    // Change language
    UnicodeString language = ComboBox1->Items->Strings[ComboBox1->ItemIndex];
    const int CROATIAN = (SUBLANG_CROATIAN_CROATIA << 10) | LANG_CROATIAN;
    const int ENGLISH = (SUBLANG_ENGLISH_US << 10) | LANG_ENGLISH;
    ds_user->Enabled = false;
    table_user->Active = false;
    ado_connection->Connected = false;
    if(language == "CRO") {
        if(LoadNewResourceModule(CROATIAN)) {
            ReinitializeForms();
        }
	} else {
        ado_connection->Connected = false;
        if(LoadNewResourceModule(ENGLISH)) {
            ReinitializeForms();
        }
        ado_connection->Connected = true;
	}
    ado_connection->Connected = true;
    table_user->Active = true;
    ds_user->Enabled = true;
    control_image->Hint = Button2->Caption;
    profile_combo->ItemIndex = 0;
}
