#include <vcl.h>
#pragma hdrstop

#include "scoresForm.h"
#include "scores.h"
#include <math.h>
#include <string>
#include <iomanip> // setprecision
#include <sstream> // stringstream
#include <string>
#include <StrUtils.hpp>

#pragma package(smart_init)
#pragma resource "*.dfm"
Tscore_form *score_form;

__fastcall Tscore_form::Tscore_form(TComponent* Owner): TForm(Owner) {
    ado_connection->ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=results.mdb;Mode=Share Deny None;Persist Security Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False;";
    ado_connection->Connected = true;
    tbl_users->Active = true;
    tbl_difficulties->Active = true;
    tbl_scores->Active = true;
    ds_users->Enabled = true;
}

void Tscore_form::reloadData() {
    ListView1->Items->Clear();
    int i;
    // Filter the data
    UnicodeString filterString;
    bool empty[3];
    int length = 0;
    for(i = 0; i < filterParameters.size(); i++) {
        if(filterParameters[i] != "") {
            length++;
            empty[i] = false;
		} else {
            empty[i] = true;
		}
	}
    if(length != 0) {
        tbl_scores->Filtered = false;
        if(length == 1) {
            for(i = 0; i < 3; i++) {
                if(!empty[i]) {
                    filterString = filterParameters[i];
				}
			}
		} else if(length == 2) {
            for(i = 0; i < 3; i++) {
                if(!empty[i] && filterString == "") {
                    filterString = filterParameters[i];
                } else if(!empty[i] && filterString != "") {
                    filterString += " AND " + filterParameters[i];
				}
			}
        } else {
            filterString = filterParameters[0] + " AND " + filterParameters[1] + " AND " + filterParameters[2];
		}
        tbl_scores->Filter = filterString;
        tbl_scores->Filtered = true;
	} else {
        tbl_scores->Filtered = false;
	}
    TLocateOptions options;
    options << loCaseInsensitive;
    tbl_scores->First();
    // Load the score information
    for(i = 0; i < tbl_scores->RecordCount; i++) {
        ListView1->Items->Add();
        // Get the data
        int difficultyID = tbl_scores->FieldByName("DifficultyID")->AsInteger;
        int userID = tbl_scores->FieldByName("UserID")->AsInteger;
        float time = tbl_scores->FieldByName("Time")->AsFloat;
        // Round to two decimals
        UnicodeString timeString = FloatToStr(time);
        std::stringstream stream;
        stream << std::fixed << std::setprecision(2) << time;
        std::string finalString = stream.str();
        // Add the time
        ListView1->Items->Item[i]->Caption = finalString.c_str();
        // Add the username
        if(tbl_users->Locate("ID", userID, options) == true) {
            ListView1->Items->Item[i]->SubItems->Add(tbl_users->FieldByName("Username")->AsString);
		}
        // Add the difficulty
        if(tbl_difficulties->Locate("ID", difficultyID, options) == true) {
            ListView1->Items->Item[i]->SubItems->Add(tbl_difficulties->FieldByName("Description")->AsString + Button2->Caption);
        }
        tbl_scores->Next();
    }
}

void __fastcall Tscore_form::FormCreate(TObject *Sender) {
    // Resize the vector to accomodate the filter parameters
    // 0 - Time, 1 - Username, 2 - Difficulty
    filterParameters.resize(3);
    int i;
    // Usernames
    tbl_users->First();
    ComboBox1->Items->Add(Button1->Caption);
	for(i = 0; i < tbl_users->RecordCount; i++) {
        ComboBox1->Items->Add(tbl_users->FieldByName("Username")->AsString);
        tbl_users->Next();
	}
    ComboBox1->ItemIndex = 0;
    // Difficulties
    tbl_difficulties->First();
    ComboBox2->Items->Add(Button1->Caption);
	for(i = 0; i < tbl_difficulties->RecordCount; i++) {
        ComboBox2->Items->Add(tbl_difficulties->FieldByName("Description")->AsString);
        tbl_difficulties->Next();
	}
    ComboBox2->ItemIndex = 0;
    tbl_scores->IndexFieldNames = "Time ASC";
    reloadData();
    ComboBox3->ItemIndex = 0;
}

void __fastcall Tscore_form::FormClose(TObject *Sender, TCloseAction &Action) {
    //
}

void __fastcall Tscore_form::ComboBox1Change(TObject *Sender) {
    TLocateOptions options;
    options << loCaseInsensitive;
    // Filter by player (username)
    UnicodeString selectedItem = ComboBox1->Items->Strings[ComboBox1->ItemIndex];
    if(selectedItem == "Any" || selectedItem == "Sve") {
        // Remove the filtering
        filterParameters[1] = "";
	} else {
        // Find the username ID
        if(tbl_users->Locate("Username", selectedItem, options) == true) {
            filterParameters[1] = "UserID = " + IntToStr(tbl_users->FieldByName("ID")->AsInteger);
        }
	}
    reloadData();
}

void __fastcall Tscore_form::ComboBox2Change(TObject *Sender) {
    TLocateOptions options;
    options << loCaseInsensitive;
    // Filter by difficulty
    UnicodeString selectedItem = ComboBox2->Items->Strings[ComboBox2->ItemIndex];
    if(selectedItem == "Any" || selectedItem == "Sve") {
        // Remove the filtering
        filterParameters[2] = "";
	} else {
        // Find the difficulty ID
        if(tbl_difficulties->Locate("Description", selectedItem, options) == true) {
            filterParameters[2] = "DifficultyID = " + IntToStr(tbl_difficulties->FieldByName("ID")->AsInteger);
        }
	}
    reloadData();
}

void __fastcall Tscore_form::Edit1Change(TObject *Sender) {
    // Filter by time
    UnicodeString timeString = Edit1->Text;
    if(timeString == "") {
        // Remove filtering by time
        filterParameters[0] = "";
	} else {
        // Do the filtering
        filterParameters[0] = "Time < " + timeString;
	}
    reloadData();
}

void __fastcall Tscore_form::ComboBox3Change(TObject *Sender) {
    UnicodeString orderType = ComboBox4->Items->Strings[ComboBox4->ItemIndex];
    UnicodeString columnName = ComboBox3->Items->Strings[ComboBox3->ItemIndex];
    if(columnName == "Difficulty" || columnName == "Tezina igranja") {
        columnName = "DifficultyID";
	} else {
        columnName = "Time";
	}
    tbl_scores->IndexFieldNames = columnName + " " + orderType;
    reloadData();
}

void __fastcall Tscore_form::ComboBox4Change(TObject *Sender) {
    // Change type of column ordering
    String information = tbl_scores->IndexFieldNames;
    TStringDynArray explode = Strutils::SplitString(information, " ");
    UnicodeString columnName = explode[0];
    UnicodeString orderType = ComboBox4->Items->Strings[ComboBox4->ItemIndex];
    tbl_scores->IndexFieldNames = explode[0] + " " + orderType;
    reloadData();
}
