#include <vcl.h>
#pragma hdrstop

#include "login_window.h"
#include "input_form.h"
#include "ctype.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tlogin_form *login_form;

void __fastcall Tlogin_form::update_combo() {
	int remember = profile_combo->ItemIndex;
	profile_combo->Clear();
	table_user->First();
  	for(int i = 0; i < table_user->RecordCount; i++) {
		profile_combo->Items->Add(table_user->FieldByName("Username")->Value);
		table_user->Next();
  	}
	profile_combo->ItemIndex = remember;
}

__fastcall Tlogin_form::Tlogin_form(TComponent* Owner): TForm(Owner) {
    control_image->ShowHint = true;
    control_image->Hint = "Empty input.";
    control_image->Picture->LoadFromFile("red.png");
	valid = false;
}

void __fastcall Tlogin_form::validate() {
	UnicodeString empty_message = "Empty input ";
	UnicodeString length_message = "Invalid length ";
	UnicodeString character_message = "Invalid characters ";
	UnicodeString exists_message = "User already exists ";
	UnicodeString empty = "";

    UnicodeString username = Trim(username_edit->Text);

    bool invalid_length = false;
    bool empty_input = false;
    bool invalid_characters = false;
	bool exists_in_database = false;

	TLocateOptions Opts;     
	Opts.Clear();
	Opts << loCaseInsensitive; 
	if(table_user->Locate("Username", username, Opts)) {
		exists_in_database = true;
    }
	
    if(username == "") {
        empty_input = true;
	}

    if(username.Length() > 12 || username.Length() < 3) {
		invalid_length = true;
	}

	for(int i = 1; i <= username.Length(); i++) {
    	if(!isalnum(username[i])) {
        	invalid_characters = true;
			break;
		}
	}

	if(empty_input || invalid_length || invalid_characters || exists_in_database) {
		UnicodeString hint_message = 
			((empty_input) ? empty_message : empty) +
			((invalid_length) ? length_message : empty) +
        	((invalid_characters) ? character_message : empty) +
			((exists_in_database) ? exists_message : empty);
		valid = false;
		control_image->Hint = hint_message;
		control_image->Picture->LoadFromFile("red.png");
	} else {
		valid = true;
		control_image->Hint = "Input is valid.";
		control_image->Picture->LoadFromFile("green.png");
	}
}

void __fastcall Tlogin_form::username_editChange(TObject *Sender) {
	validate();
}

void __fastcall Tlogin_form::FormCreate(TObject *Sender) {
  	update_combo();
    profile_combo->ItemIndex = 0;
}

void __fastcall Tlogin_form::create_buttonClick(TObject *Sender) {
	if(valid) {
		UnicodeString username = Trim(username_edit->Text);
		table_user->Insert();
    	table_user->FieldByName("Username")->AsString = username;
    	table_user->Post();
		update_combo();
		validate();
	} else {
    	ShowMessage("Invalid input!");
	}	
}

void __fastcall Tlogin_form::login_buttonClick(TObject *Sender) {
	Tsize_form *size_form = new Tsize_form(this, profile_combo->Items->Strings[profile_combo->ItemIndex]);
	size_form->Show();
    this->Close();
	delete size_form;
}                                                                        
