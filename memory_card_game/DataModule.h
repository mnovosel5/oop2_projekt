//---------------------------------------------------------------------------

#ifndef DataModuleH
#define DataModuleH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
//---------------------------------------------------------------------------
class TMemoryDataModule : public TDataModule
{
__published:	// IDE-managed Components
	TADOConnection *Connection;
	TADOTable *TableUsers;
	TADOTable *TableDifficulties;
	TADOTable *TableScores;
	TDataSource *DataUsers;
	TDataSource *DataDifficulties;
	TDataSource *DataScores;
private:	// User declarations
public:		// User declarations
	__fastcall TMemoryDataModule(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMemoryDataModule *MemoryDataModule;
//---------------------------------------------------------------------------
#endif
