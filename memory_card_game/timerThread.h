#ifndef timerThreadH
#define timerThreadH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Dialogs.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>


class TimerThread : public TThread {
    private:
    protected:
        void __fastcall Execute();
    public:
        __fastcall TimerThread(bool CreateSuspended, TEdit *_editReference, bool _english);
        void __fastcall timerTrigger(TObject *Sender);
        int elapsedTime;
        TEdit *editReference;
        TTimer *timer;
        bool english;
};

#endif
